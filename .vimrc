set nocompatible " be iMproved, required
filetype off     " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" Plugins 
Plugin 'tpope/vim-fugitive'
Plugin 'lokaltog/vim-easymotion'
Plugin 'bling/vim-airline'
Plugin 'Valloric/YouCompleteMe'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'airblade/vim-gitgutter'
Plugin 'jiangmiao/auto-pairs'
Plugin 'amiorin/vim-project'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'paradigm/vim-multicursor'
Plugin 'majutsushi/tagbar'
Plugin 'xolox/vim-misc'
Plugin 'msanders/snipmate.vim'
Plugin 'qpkorr/vim-bufkill'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'vim-scripts/SQLUtilities'
Plugin 'vim-scripts/Align'
Plugin 'scrooloose/syntastic'
Plugin 'vim-scripts/vimprj'
Plugin 'vim-scripts/DfrankUtil'
Plugin 'vim-scripts/indexer.tar.gz'
Plugin 'shawncplus/phpcomplete.vim'
Plugin 'arnaud-lb/vim-php-namespace'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'morhetz/gruvbox'
Plugin 'terryma/vim-multiple-cursors'

" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on


" Common settings
syntax on
set encoding=utf-8
set hidden
set noshowmode
set number
set confirm
set autowrite
set scrolloff=5
set expandtab
set copyindent
set preserveindent
set softtabstop=0
set shiftwidth=4
set tabstop=4
set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=L
set shortmess+=I
" set mouse=c
set noswapfile
set cursorline
set binary
set noeol


" Common Keymaps
imap jj <Esc>
nnoremap j gj
nnoremap k gk
map <F8> :w!<cr>
nmap <leader>w :w!<cr>
map <F4> :BD<cr>
map Q <Nop> 


" Define leader key
let mapleader = ","
let g:mapleader = ","


" Copy from and paste to system clipboard 
vmap <silent> <C-c> "+y
nmap <silent> <C-v> "+p
vmap <silent> <leader>y "+y
nmap <silent> <leader>p "+y


" 80 chars border highlight
let &colorcolumn=join(range(81,999),",")
let &colorcolumn="80,".join(range(400,999),",")
set fillchars+=vert:\ 
hi ColorColumn guibg=#ffffff ctermbg=246


" Disable arrow-keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>


" Easier window navigation
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l


" Set vim's tmp dir
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup


" Set theme
let g:gruvbox_italic = 0
let g:gruvbox_contrast_dark = "hard"
colorscheme gruvbox
set bg=dark
hi NonText guifg=bg


" Airline plugin options
set guifont=Ubuntu\ Mono\ derivative\ Powerline\ 12.65
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'


" Vim-project plugin options
call project#rc("~/Projects")
let g:project_use_nerdtree = 1
set rtp+=~/.vim/bundle/vim-project/
source ~/.vim_projects


" NERDTree plugin options
let g:NERDTreeWinPos = "left"
let g:NERDTreeWinSize = 32
let g:tagbar_width = 32
let g:nerdtree_tabs_smart_startup_focus=2
let g:nerdtree_tabs_open_on_console_startup=1
let g:nerdtree_tabs_open_on_gui_startup=1
let g:nerdtree_tabs_autoclose=0
let g:nerdtree_tabs_autofind=1
let g:NERDTreeShowHidden=1
map <C-n> :NERDTreeToggle<CR>   
map <silent> <leader>r :NERDTreeFind<CR> <C-w>l
let NERDTreeIgnore = ['\.git$']
"autocmd FileType php,python,raby,js,c,cpp nested :TagbarOpen


" CTR-P plugin options
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/vendor,*/cache/*
nmap <silent> <leader><C-c> :CtrlPTag<CR>
nmap <C-c> :CtrlPBuffer<CR>


" Indexer plugin options
let g:indexer_disableCtagsWarning = 1


" Update the project tags and file index
fun! IndexerRebuildAndCtrlPClearAllCaches()
	execute 'CtrlPClearAllCaches'
	execute 'IndexerRebuild'
	echo "Cleared All Tags Caches & Rebuild Index... Done"
endfun
nnoremap <F10> :call IndexerRebuildAndCtrlPClearAllCaches()<CR>


" EasyMotion plugin options
let g:EasyMotion_leader_key = '<Leader>'


" Syntastic plugin options
map <leader>so :SyntasticToggleMode<CR>   
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_php_checkers = ['php']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height=3


" Nerdtree gitplugin
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "*",
    \ "Staged"    : "+",
    \ "Untracked" : "!",
    \ "Renamed"   : "-",
    \ "Unmerged"  : "=",
    \ "Deleted"   : "#",
    \ "Dirty"     : "*",
    \ "Clean"     : "_",
    \ "Unknown"   : "?"
\ }


" Remove trailing whitespace by vim
fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
autocmd FileType php,python,raby,js,c,cpp autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()


" Replase tabs with spaces
fun! <SID>ReplaseTabswithSpaces()
    let l = line(".")
    let c = col(".")
    %s/\t/    /egi
    call cursor(l, c)
endfun
" autocmd FileType php,python,raby,js,c,cpp autocmd BufWritePre <buffer> :call <SID>ReplaseTabswithSpaces()
" autocmd FileType * autocmd BufReadPre <buffer> :call <SID>ReplaseTabswithSpaces()


" Laravel framework commons
nmap <leader>lr :e app/Http/routes.php<cr>
nmap <leader>lca :e app/config/app.php<cr>
nmap <leader>lcd :e app/config/database.php<cr>
nmap <leader>lc :e composer.json<cr>


" Key maping 
map ё `
map й q
map ц w
map у e
map к r
map е t
map н y
map г u
map ш i
map щ o
map з p
map х [
map ъ ]
map ф a
map ы s
map в d
map а f
map п g
map р h
map о j
map л k
map д l
map ж ;
map э '
map я z
map ч x
map с c
map м v
map и b
map т n
map ь m
map б ,
map ю .
map . /
map Ё ~
map Й Q
map Ц W
map У E
map К R
map Е T
map Н Y
map Г U
map Ш I
map Щ O
map З P
map Х {
map Ъ }
map Ф A
map Ы S
map В D
map А F
map П G
map Р H
map О J
map Л K
map Д L
map Ж :
map Э "
map Я Z
map Ч X
map С C
map М V
map И B
map Т N
map Ь M
map Б <
map Ю >
map , ?

